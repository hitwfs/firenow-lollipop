/* Automatically generated - do not edit */
#define CONFIG_RKCHIP_RK3288	1
#define CONFIG_PRODUCT_BOX	1
#define CONFIG_BOARDDIR board/rockchip/rk32xx
#include <config_defaults.h>
#include <configs/rk32plat.h>
#include <asm/config.h>
#include <config_fallbacks.h>
#include <config_uncmd_spl.h>
